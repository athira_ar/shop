SHOP
-----------
This is a simple python django application.

Database
-----------
sqlite3

RUN The Application
---------------------------------------------------------

To run ,

python manage.py runserver 0.0.0.0:9000

Server will start on: 

Starting development server at http://0.0.0.0:9000/


Docker
---------
Using Dockerfile we can build and run docker image of this application.

eg: docker build -t shop:1 .

docker run -it --name shop_1 -p 9000:9000 shop:1

It will expose port number 9000.

Docker Hub
--------------

pushed the image in docker hub

You can take a pull using

docker pull 10131993/shop:1

Kubernetes
--------------

Deployment of this application using kubernetes

deployment.yaml and service.yaml is provided in the folder 'kubernetes_files'


Application deployed to the cluster with the command 
kubectl create -f deployment.yaml

Deploy the Service with 
kubectl create -f service.yaml

In katacoda, can view the application via

Display a different port
If the service is running on a different port then please enter it below

give 31000 and

click 'Display Port' button 

Admin page
--------

/admin
username: admin
password: admin@123

products page
-------------

/products

will display the product details



