FROM python:3.7
WORKDIR /app/
COPY . /app/
RUN pip3 install -r requirements.txt
RUN python manage.py migrate
CMD ["python","manage.py","runserver","0.0.0.0:9000"]

